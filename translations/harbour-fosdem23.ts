<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>About</name>
    <message>
        <source>Version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Author: Josip Delic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redesign: Joan CiberSheep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sailfish OS Port: Fabio Comuni</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>2023 update: David Llewellyn-Jones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FOSDEM23</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Official %1 website</source>
        <extracomment>Here %1 is FOSDEM name (Should read as &apos;Official FOSDEM website&apos;)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code available on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Powered by &lt;b&gt;PyOtherSide&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Checked</name>
    <message>
        <source>in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No favourites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activate a star on a lecture to add it here</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <source>Guestbook comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommonPullDown</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Guest book</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FOSDEM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadDialog</name>
    <message>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All &apos;Checked&apos; entries will be deleted!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>URL of Fosdem Pentabarf XML</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GuestBook</name>
    <message>
        <source>FOSDEM&apos;23 Guestbook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add your name to the Linux on Mobile FOSDEM&apos;23 Guestbook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>I was there!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Instructions</name>
    <message>
        <source>Find us at the Linux on Mobile stand
Building H Stand 6

Then:
	1. Find GuestBook.qml in Sailfish IDE
	2. Make a copy of a GuestBookEntry
	3. Edit the name and comment
	4. Save the file (Ctrl-S)

Watch as QML Live automatically updates the app in realtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instructions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Press me for instructions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Follow these simple steps</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Lecture</name>
    <message>
        <source>Lecture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&apos;No lecture info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Overlaps with %1 checked lectures:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Sun</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fri</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Map</name>
    <message>
        <source>Coordinates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Overlaps</name>
    <message>
        <source>Overlaps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No overlaps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>S:</name>
    <message>
        <source></source>
        <comment>Here %1 is FOSDEM name (Should read as &apos;Official FOSDEM website&apos;)</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>Schedule</name>
    <message>
        <source>Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&apos;No schedule info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TopNotification</name>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An error has occured: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Track</name>
    <message>
        <source>Empty database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select Import from the menu to load the schedule</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
