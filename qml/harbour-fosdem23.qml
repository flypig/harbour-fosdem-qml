import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "pages"
import "components"

ApplicationWindow {
    objectName: "mainView"
    id: mainView

    property var path: []
    property string xmlsource
    property Page _mainPage

    signal xmlChanged
    signal favouritesChanged

    cover: Qt.resolvedUrl("cover/CoverPage.qml")

    Python {
        id: py
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('./'));

            importModule('backend', function() {
                console.log("DEBUG: python loaded");
            });

            setHandler('on-progress', function(value) {
                if (!progressBar.visible) {
                    progressBar.visible = true;
                }
                progressBar.value = Math.ceil(value * 2);
            });
        }

        onError: {
            console.log('Error: ' + traceback);
            errorDialog.error(traceback);
        }
    }

    ProgressBar {
        id: progressBar
        minimumValue: 0
        maximumValue: 100
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        visible: false
    }

    TopNotification {
        id: errorDialog
    }

    Component.onCompleted: {
        _createMainPage()
        py.call("backend.file_exists", [], load_schedule)
    }

    function download_end(data) {
        progressBar.visible = false
        py.call("backend.get_schedule_file_path",  [true], function(path) {
            xmlsource = path;
            xmlChanged();
        })
    }

    function load_schedule(exists) {
        if (!exists) {
            var dialog = pageStack.push(Qt.resolvedUrl("pages/DownloadDialog.qml"), {"backNavigation": false});
        }
        else {
            py.call("backend.get_schedule_file_path",  [], function(path) {
                xmlsource = path;
                xmlChanged();
            })
        }
    }

    function toDateString(datestr) {
        return toDate(datestr).toDateString()
    }

    function toDate(datestr) {
        return new Date(datestr);
    }

    function now() {
        return new Date();
    }

    function navigateToLecture(lecture) {
        _resetPageStack()
        pageStack.push(Qt.resolvedUrl("pages/Lecture.qml"),
                       {"model": lecture}, PageStackAction.Immediate)
    }

    function _createMainPage() {
        _resetPageStack()
        _mainPage = pageStack.replace(Qt.resolvedUrl("pages/MainPage.qml"),
                                      {}, PageStackAction.Immediate)
    }

    function _resetPageStack() {
        if (pageStack.currentPage != _mainPage) {
            pageStack.pop(_mainPage, PageStackAction.Immediate)
        }
    }
}
