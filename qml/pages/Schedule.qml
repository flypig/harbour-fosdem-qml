import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: schedule

    property var model: ListModel {}
    property string track: ""

    SilicaListView {
        anchors.fill: parent
        header: PageHeader {
            title: qsTr("Schedule")
            description: track
        }
        model: schedule.model
        delegate: scheduleDelegate

        PullDownMenu {
            MenuItem {
                id: mapMenu
                text: qsTr("Map")
                onClicked: pageStack.push(Qt.resolvedUrl("Map.qml"));
            }
        }

        ViewPlaceholder {
            enabled: schedule.model.count === 0
            text: qsTr("'No schedule info")
        }

        VerticalScrollDecorator {}
    }

    Component {
        id: scheduleDelegate

        ListItem {
            id: delegate

            anchors {
                left: parent.left
                right: parent.right
            }
            contentHeight: row.height + (Theme.paddingMedium * 2)

            Row {
                id: row

                spacing: Theme.paddingSmall
                anchors {
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                    topMargin: Theme.paddingSmall
                    left: parent.left
                    right: parent.right
                    top:parent.top
                }

                Column {
                    id: column

                    spacing: Theme.paddingSmall
                    width: parent.width - lecture_favourite.width - Theme.paddingSmall

                    Label {
                       text: model.title
                       width: parent.width
                       wrapMode: Text.WordWrap
                       color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
                    }

                    Label {
                        text: "[" + toDateString(day) +"] " + start + " - " + end + " " + qsTr('in') + " " + room
                        width: parent.width
                        wrapMode: Text.WordWrap
                        color: delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                        font.pixelSize: Theme.fontSizeTiny
                    }
                }

                Favorite {
                    id: lecture_favourite

                    width: Theme.iconSizeMedium
                    height: width
                    checked: lecture_checked

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            py.call('backend.toggle', [model], function (data) {
                                model.lecture_checked = data;
                            });
                            mainView.favouritesChanged()
                        }
                    }
                }
            }

            onClicked: {
                pageStack.push(Qt.resolvedUrl("Lecture.qml"), {"model": schedule.model.get(index)})
            }
        }
    }
}
