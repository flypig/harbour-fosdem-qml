import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Silica.private 1.0
import "../components"

TabItem {
    objectName: "Checked"
    id: checked

    property var model: ListModel {}
    property real topMargin

    anchors.fill: parent
    flickable: checkedlistView

    Component.onCompleted: checked.updateFavourites()

    Connections {
        target: mainView

        onFavouritesChanged: checked.updateFavourites()
    }

    function updateFavourites() {
        checked.model.clear();
        py.call("backend.select_all", [], function (events) {
            for (var i=0; i < events.length; i++) {
                checked.model.append(events[i]);
            }
        });
    }

    SilicaListView {
        id: checkedlistView

        anchors.fill: parent

        header: Item {
            width: 1
            height: checked.topMargin
        }

        CommonPullDown {
        }

        VerticalScrollDecorator {}

        model: checked.model
        delegate: checkedDelegate

        ViewPlaceholder {
            enabled: checked.model.count === 0
            text: qsTr("No favourites")
            hintText: qsTr("Activate a star on a lecture to add it here")
        }
    }

    Component {
        id: checkedDelegate

        ListItem {
            id: delegate

            opacity: (now() > toDate(datetime_end)) ? 0.5 : 1

            anchors {
                left: parent.left
                right: parent.right
            }
            contentHeight: row.height + (Theme.paddingSmall * 2)

            Row {
                id: row

                spacing: Theme.paddingSmall
                width: parent.width - 2 * Theme.horizontalPageMargin
                x: Theme.horizontalPageMargin

                Column {
                    id: column
                    spacing: Theme.paddingSmall
                    width: parent.width - saved_lecture.width - Theme.paddingSmall

                    Label {
                       text: model.title
                       width: parent.width
                       wrapMode: Text.WordWrap
                       color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
                    }

                    Label {
                        text: "[" + toDateString(day) +"] " + start + " - " + end + " " + qsTr('in') + " " + room
                        width: parent.width
                        wrapMode: Text.WordWrap
                        color: delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                        font.pixelSize: Theme.fontSizeTiny
                    }
                }

                Favorite {
                    id: saved_lecture

                    width: Theme.iconSizeMedium
                    height: width
                    checked: lecture_checked
                }
            }

            onClicked: {
                pageStack.push(Qt.resolvedUrl("Lecture.qml"), {"model": checked.model.get(index)})
            }
        }
    }

}
