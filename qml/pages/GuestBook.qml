import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

// Instructions: follow these simple steps.
//
//  1. Find GuestBook.qml (the file you're reading!) in the Sailfish IDE.
//  2. Make a copy of a GuestBookEntry components.
//  3. Edit your name and comment to your liking.
//  4. Save the file (Ctrl-S).
//
//  Watch as QML Live automatically updates the app in realtime.

Page {
    id: page

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: col.height + Theme.paddingLarge

        Column {
            id: col

            spacing: Theme.paddingLarge
            width: parent.width

            Image { source: Qt.resolvedUrl("../images/lom.png") }

            PageHeader { title: qsTr("FOSDEM'23 Guestbook") }

            Label {
                text: qsTr("Add your name to the Linux on Mobile FOSDEM'23 Guestbook")
                x: Theme.horizontalPageMargin
                width: parent.width - 2 * x
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: Theme.secondaryHighlightColor
            }

            Instructions {}

            SectionHeader { text: qsTr("I was there!") }

            Column {
                width: parent.width

                // Add a new entry here by copying a GuestBookEntry and editing it
                // ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓

                GuestBookEntry {
                    name: "David Llewellyn-Jones"
                    comment: "Writing this in advance!"
                }

                GuestBookEntry {
                    name: "Yvan Sraka"
                    comment: "<3 Qt"
                }
                GuestBookEntry {
                    name: "Angelo Rizzi"
                    comment: "<3 Sailfish Os"
                }
                GuestBookEntry {
                    name: "SfietKonstantin"
                    comment: "Hello !"
                }
                GuestBookEntry {
                    name: "rubdos"
                    comment: "Hello from Belgium!"
                }
                GuestBookEntry {
                    name: "jmlich"
                    comment: "Hello from NemoMobile project..."
                }
                GuestBookEntry {
                    name: "Nico"
                    comment: "Thank you for the great OS starting at the J1!"
                }
                GuestBookEntry {
                    name: "Chris/ahappyhuman"
                    comment: "With love from Holland"
                }
                GuestBookEntry {
                    name: "Luis/ahappyhuman"
                    comment: "With love from France and Brazil"
                }
                GuestBookEntry {
                    name: "@denise:exarius.org"
                    comment: "Thanks for the demo, good luck!"
                }
                GuestBookEntry {
                    name: "Alex/zombie"
                    comment: "Arrrr from Romania"
                }
                GuestBookEntry{
                    name: "toby"
                    comment: "spaces before braces??"
                }

                GuestBookEntry {
                    name: "Angelo/ Neapolitan"
                    comment: "Never give up"
                }
                GuestBookEntry {
                    name: "Viktors"
                    comment: "Greetings from Latvia :)"
                }
                GuestBookEntry {
                    name: "FOSSapphire"
                    comment: "Hello"
                }
                GuestBookEntry {
                    name: "LINMOB"
                    comment: "Keep it up!"
                }
                GuestBookEntry {
                    name: "bionade24"
                    comment: "This is a cool easter egg!"
                }
                GuestBookEntry {
                    name: "Jens"
                    comment: "Open source phones - super cool!"
                }
                GuestBookEntry {
                    name: "cybette"
                    comment: "Sailfish 4evr!"
                }
                GuestBookEntry {
                    name: "Chris Simmonds"
                    comment: "Sail on Jolla"
                }
                GuestBookEntry {
                    name: "Andrew"
                    comment: "20 GOTO 10"
                }
                GuestBookEntry {
                    name: "@calum@fosstodon.org"
                    comment: "Looks great. Cant wait until Ubuntu Touch is practical for everyday use"
                }
                GuestBookEntry {
                    name: "@b-barry"
                    comment: "React before react"
                }
                GuestBookEntry {
                    name: "@juanc"
                    comment: "The web experience in native :)"
                }
                GuestBookEntry {
                    name: "@josuemotte"
                    comment: "Finally an alternative!"
                }
                GuestBookEntry {
                    name: "nico"
                    comment: "great event, good to see a lot of Jolla folks and SFOS enthusiasts here!"
                }
                GuestBookEntry {
                    name: "romni"
                    comment: "good looking OS! thanks for the hard work :)"
                }
                GuestBookEntry {
                    name: "Nathan"
                    comment: "Noice!!!!"
                }
                GuestBookEntry {
                    name: "Fabrixxm"
                    comment: "Back to FOSDEM!!1!!!"
                }
                GuestBookEntry {
                    name: "zev"
                    comment: "Hi from NetXMS!"
                }
                GuestBookEntry {
                    name: "niki"
                    comment: "Keep up the good work!! -- A very pleased user"
                }
                GuestBookEntry {
                    name: "cLx"
                    comment: "Meow! Good to see you at FOSDEM! Plz continue to keeping us away from GAFAM mobiles solutions ^^"
                }
                GuestBookEntry {
                    name: "WebGang"
                    comment: "Gladd to meet the Sailfish people (they fixed a small problom on my sailfish as well) at Fosdem 2023"
                }
                GuestBookEntry {
                    name: "imega"
                    comment: "Great to see the improvement of opensource phone OSs!"
                }

            }
        }

        VerticalScrollDecorator {}
    }
}
