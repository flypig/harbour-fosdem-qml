import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id: dialog

    Column {
        width: parent.width

        spacing: Theme.paddingLarge

        DialogHeader {
            title: qsTr("Download")
        }

        Label {
            text: qsTr("All 'Checked' entries will be deleted!")
            font.bold: true
            width: parent.width - 2 * x
            x: Theme.horizontalPageMargin
        }

        TextField {
            id: urlField

            text: {
                var year = new Date().getFullYear();
                return "https://fosdem.org/" + year + "/schedule/xml"
            }
            width: parent.width
            label: qsTr("URL of Fosdem Pentabarf XML")
            labelVisible: true

        }

    }

    onDone: {
        if (result == DialogResult.Accepted) {
            var url = urlField.text;
            py.call("backend.download_file", [url], download_end);
        }
    }

}
