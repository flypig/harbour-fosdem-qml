import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Silica.private 1.0
import Sailfish.Gallery 1.0

Page {
    id: map

    anchors.fill: parent

    SilicaFlickable {
        id: flickable

        anchors.fill: parent

        PullDownMenu  {
            MenuItem {
                text: qsTr("Coordinates")
                onClicked: Qt.openUrlExternally("geo:50.8124,4.3807")
            }
        }

        ImageViewer {
            id: mapimage

            anchors.fill: parent
            source: "../images/campus.png"
        }
    }
}
