import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Silica.private 1.0
import QtQuick.XmlListModel 2.0

Page {
    id: mainpage

    property alias tabsModel: tabs.model

    TabView {
        id: tabs

        anchors.fill: parent
        currentIndex: 0

        header: TabBar {
            model: tabBarModel
        }

        model: []
    }

    Component {
        id: dayOneView

        Track {
            dayIndex: 0
            xmlsource: mainView.xmlsource
            topMargin: tabs.tabBarHeight
        }
    }

    Component {
        id: dayTwoView

        Track {
            dayIndex: 1
            xmlsource: mainView.xmlsource
            topMargin: tabs.tabBarHeight
        }
    }

    Component {
        id: dayThreeView

        Track {
            dayIndex: 2
            xmlsource: mainView.xmlsource
            topMargin: tabs.tabBarHeight
        }
    }

    Component {
        id: favView

        Checked {
            topMargin: tabs.tabBarHeight
        }
    }

    ListModel {
        id: tabBarModel
    }

    function updateDayModel() {
        // Dynamically create the tabs based on the downloaded data
        var weekDays = [qsTr("Sun"),
                        qsTr("Mon"),
                        qsTr("Tue"),
                        qsTr("Wed"),
                        qsTr("Thur"),
                        qsTr("Fri"),
                        qsTr("Sat")]
        var tabComponents = [dayOneView, dayTwoView, dayThreeView]
        tabBarModel.clear()
        var tabsModel = []
        for (var i = 0; i < Math.min(daymodel.count, tabComponents.length); ++i) {
            var weekDay = weekDays[toDate(daymodel.get(i).date).getDay()]
            tabBarModel.append({"title": weekDay})
            tabsModel.push(tabComponents[i])
        }
        tabBarModel.append({"title": "Favourites"})
        tabsModel.push(favView)

        tabs.model = tabsModel
    }

    Connections {
        target: daymodel

        onStatusChanged: {
            if (daymodel.status == XmlListModel.Ready) {
                updateDayModel()
            }
        }
    }

    Connections {
        target: mainView

        onXmlChanged: {
            daymodel.reload()
        }
    }

    XmlListModel {
        id: daymodel

        source: mainView.xmlsource
        query: "/schedule/day"

        XmlRole {
            name: "date"
            query: "@date/string()"
        }
    }
}
