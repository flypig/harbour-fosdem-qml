import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    property alias name: nameLabel.text
    property alias comment: commentLabel.text
    property alias title: header.title

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: col.height + Theme.paddingLarge

        Column {
            id: col

            spacing: Theme.paddingLarge
            width: parent.width - 2 * x
            x: Theme.horizontalPageMargin

            PageHeader {
                id: header
                title: qsTr("Guestbook comment")
            }

            Label {
                id: nameLabel

                width: parent.width
                font.pixelSize: Theme.fontSizeLarge
                truncationMode: TruncationMode.Fade
                wrapMode: Text.Wrap
                color: Theme.highlightColor
            }

            Label {
                id: commentLabel
                width: parent.width
                font.pixelSize: Theme.fontSizeMedium
                wrapMode: Text.Wrap
                color: Theme.secondaryHighlightColor
            }
        }
    }
}
