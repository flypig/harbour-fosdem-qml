import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    objectName: "Overlaps"
    id: overlaps

    property var item
    property var model: ListModel {}

    Component.onCompleted: {
        overlaps.model.clear()
        overlaps.model.append(overlaps.item)
        py.call('backend.get_overlap', [overlaps.item], function (events) {
            for (var i=0; i < events.length; i++) {
                overlaps.model.append(events[i]);
            }
        });
    }

    SilicaListView {
        id: overlapslistView

        anchors.fill: parent
        model: overlaps.model
        delegate: overlapsDelegate

        header: PageHeader {
            title: qsTr('Overlaps')
        }

        PullDownMenu {
            MenuItem {
                id: mapMenu
                text: qsTr("Map")
                onClicked: pageStack.push(Qt.resolvedUrl("Map.qml"));
            }
        }

        ViewPlaceholder {
            enabled: overlaps.model.count === 0
            text: qsTr("No overlaps")
        }

        VerticalScrollDecorator {}
    }

    Component {
        id: overlapsDelegate

        ListItem {
            id: delegate

            opacity: (now() > toDate(datetime_end)) ? 0.5 : 1
            anchors {
                left: parent.left
                right: parent.right
            }
            contentHeight: column.height + (Theme.paddingSmall * 2)

            Column {
                id: column

                spacing: Theme.paddingSmall
                width: parent.width
                anchors {
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                    topMargin: Theme.paddingSmall
                    left: parent.left
                    right: parent.right
                    top:parent.top
                }

                Label {
                    text: model.title
                    width: parent.width
                    wrapMode: Text.WordWrap
                    color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
                }

                Label {
                    text: "[" + toDateString(day) +"] " + start + " - " + end + " " + qsTr('in') + " " + room
                    width: parent.width
                    wrapMode: Text.WordWrap
                    color: delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                    font.pixelSize: Theme.fontSizeTiny
                }
            }

            onClicked: {
                pageStack.push(Qt.resolvedUrl("Lecture.qml"), {"model": overlaps.model.get(index)})
            }
        }
    }
}

