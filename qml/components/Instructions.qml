import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    property alias name: nameLabel.text
    property string comment: qsTr("Find us at the Linux on Mobile stand\n"
                                  + "Building H Stand 6\n\nThen:\n"
                                  + "\t1. Find GuestBook.qml in Sailfish IDE\n"
                                  + "\t2. Make a copy of a GuestBookEntry\n"
                                  + "\t3. Edit the name and comment\n"
                                  + "\t4. Save the file (Ctrl-S)\n\n"
                                  + "Watch as QML Live automatically updates the app in realtime")

    width: parent.width
    contentHeight: col.height + 2 * Theme.paddingLarge

    Column {
        id: col

        spacing: Theme.paddingSmall
        width: parent.width
        y: Theme.paddingLarge

        Label {
            id: nameLabel
            x: Theme.horizontalPageMargin
            width: parent.width - 2 * x
            text: qsTr("Instructions")
        }

        Label {
            id: commentLabel
            x: Theme.horizontalPageMargin
            width: parent.width - 2 * x
            font.pixelSize: Theme.fontSizeSmall
            color: Theme.secondaryColor
            truncationMode: TruncationMode.Fade
            maximumLineCount: 1
            text: qsTr("Press me for instructions")
        }
    }

    onClicked: {
        pageStack.animatorPush(Qt.resolvedUrl("../pages/Comment.qml"), {
                                   name: qsTr("Follow these simple steps"),
                                   comment: comment,
                                   title: qsTr("Instructions")
                               })
    }
}
