import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    property alias name: nameLabel.text
    property alias comment: commentLabel.text

    width: parent.width
    contentHeight: col.height + 2 * Theme.paddingLarge

    Column {
        id: col
        spacing: Theme.paddingSmall
        width: parent.width
        y: Theme.paddingLarge

        Label {
            id: nameLabel
            x: Theme.horizontalPageMargin
            width: parent.width - 2 * x
        }

        Label {
            id: commentLabel
            x: Theme.horizontalPageMargin
            width: parent.width - 2 * x
            font.pixelSize: Theme.fontSizeSmall
            color: Theme.secondaryColor
            truncationMode: TruncationMode.Fade
            wrapMode: Text.WordWrap
            maximumLineCount: 2
        }
    }

    onClicked: {
        pageStack.animatorPush(Qt.resolvedUrl("../pages/Comment.qml"), {
                                   name: name,
                                   comment: comment
                               })
    }
}
