import os

def _get_xdg_cache_dir(org_id, app_id):
    """
    Return the XDG cache directory.
    See https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
    """
    cache_dir = os.environ.get('XDG_DATA_HOME')
    if not cache_dir:
        cache_dir = os.path.expanduser('~/.local/share')
        if cache_dir.startswith('~/'):  # Expansion failed.
            cache_dir = os.environ.get('HOME') # If expansion fail, this will fail too, probably..
    cache_dir = os.path.join(cache_dir, org_id)
    cache_dir = os.path.join(cache_dir, app_id)
    return cache_dir


APP_ID = "harbour-fosdem23"
ORG_ID = "uk.co.flypig"
DIR = _get_xdg_cache_dir(ORG_ID, APP_ID)
FILENAME = os.path.join(DIR, "schedule.xml")
DB_FILE = os.path.join(DIR, "saved.db")
